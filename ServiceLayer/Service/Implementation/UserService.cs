﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using RepositoryLayer;
using ServiceLayer.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using X.PagedList;

namespace ServiceLayer.Service.Implementation
{
    public class UserService : IUser
    {
        private readonly AppDbContext _dbContext;
        public UserService(AppDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        //Get All Users
        public PagedList<User> GetAllRepo()
        {
           PagedList<User>  user = (PagedList<User>)this._dbContext.tblUser.ToPagedList();
            return user;
        }

        //Add User
        public string AddUserRepo(User user)
        {
            try
            {
                this._dbContext.tblUser.Add(user);
                this._dbContext.SaveChanges();
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
        }

        //Get Single User
        public User GetSingleRepo(int id)
        {
            return this._dbContext.tblUser.Find(id);
        }

        public string RemoveUserRepo(int id)
        {
            try
            {
                var user = this._dbContext.tblUser.Find(id);
                this._dbContext.Remove(user);
                this._dbContext.SaveChanges();
                return "Successfully Removed";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string UpdateUserRepo(User user)
        {
            try
            {
                var userValue = this._dbContext.tblUser.Find(user.userId);
                if (userValue != null)
                {
                    userValue.userName = user.userName;
                    return "Successfully Updated";
                }
                else
                {
                    return "No Record(s) Found";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
        }
    }
}
