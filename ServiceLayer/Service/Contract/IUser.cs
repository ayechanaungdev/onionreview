﻿using DomainLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using X.PagedList;

namespace ServiceLayer.Service.Contract
{
    public interface IUser
    {
        //GetAll Record
        PagedList<User> GetAllRepo();
        //GetSingle Record
        User GetSingleRepo(int id);
        //Add Record
        string AddUserRepo(User user);
        //Update or Edit Record
        string UpdateUserRepo(User user);
        //Delete or Remove Record
        string RemoveUserRepo(int id);
    }
}
