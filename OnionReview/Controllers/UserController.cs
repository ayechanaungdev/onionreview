﻿using DomainLayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceLayer.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebOnionReview.Controllers
{

    public class UserController : Controller
    {
        private readonly IUser _user;

        public UserController(IUser user)
        {
            this._user = user;
        }

        //Get All Users
        public IActionResult GetAllRecords()
        {
            var response = this._user.GetAllRepo();
            return View(response);
        }

        //Get Single Record
        [HttpGet("get")]
        public IActionResult GetSingleRecord(int id)
        {
            return Ok(this._user.GetSingleRepo(id));
        }

        //Add User
        [HttpPost("add")]
        public IActionResult AddUser(User user)
        {
            return Ok(this._user.AddUserRepo(user));
        }

        //Remove User
        public IActionResult RemoveUser(int id)
        {
            //return Ok(this._user.RemoveUserRepo(id));
            this._user.RemoveUserRepo(id);
            return RedirectToAction("GetAllRecords");
        }

        //Update User
        [HttpPut("edit")]
        public IActionResult UpdateUser(User user)
        {
            return Ok(this._user.UpdateUserRepo(user));
        }

    }
}
